----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:08:08 06/19/2014 
-- Design Name: 
-- Module Name:    RS232_echo - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity RS232_echo is
	Port ( 	TXD		: out std_logic := '1';
		 	   RXD		: in std_logic 	:= '1';
		  	   CLK		: in std_logic;
			--   LED	: out std_logic_vector(7 downto 0) := "11111111";
			   Z	: out std_logic_vector(7 downto 0);
		  	   RST		: in std_logic	:= '0');
end RS232_echo;

architecture Behavioral of RS232_echo is

component RS232RefComp
   Port (  	
			TXD 	: out	std_logic:= '1';
		 	RXD 	: in	std_logic;					
  		 	CLK 	: in	std_logic;							
			DBIN 	: in	std_logic_vector (7 downto 0);
			DBOUT	: out	std_logic_vector (7 downto 0);
			RDA	: inout	std_logic;							
			TBE	: inout	std_logic:= '1';				
			RD		: in	std_logic;							
			WR		: in	std_logic;							
			PE		: out	std_logic;							
			FE		: out	std_logic;							
			OE		: out	std_logic;											
			RST	: in	std_logic:= '0');				
end component;	

type mainState is (stReceive,stSend);

signal dbInSig	:	std_logic_vector(7 downto 0);
signal dbOutSig	:	std_logic_vector(7 downto 0);
signal rdaSig	:	std_logic;
signal tbeSig	:	std_logic;
signal rdSig	:	std_logic;
signal wrSig	:	std_logic;
signal peSig	:	std_logic;
signal feSig	:	std_logic;
signal oeSig	:	std_logic;
	
signal stCur	:	mainState := stReceive;
signal stNext	:	mainState;

begin

	Z(7) <= dbOutSig(0);
	Z(6) <= dbOutSig(1);
	Z(5) <= dbOutSig(2);
	Z(4) <= dbOutSig(3);
	Z(3) <= dbOutSig(4);
	Z(2) <= dbOutSig(5);
	Z(1) <= dbOutSig(6);
	Z(0) <= dbOutSig(7);

	UART: RS232RefComp 
			port map (	
							TXD 	=> TXD,
							RXD 	=> RXD,
							CLK 	=> CLK,
							DBIN 	=> dbInSig,
							DBOUT	=> dbOutSig,
							RDA	=> rdaSig,
							TBE	=> tbeSig,	
							RD		=> rdSig,
							WR		=> wrSig,
							PE		=> peSig,
							FE		=> feSig,
							OE		=> oeSig,
							RST 	=> RST
						);

	process (CLK, RST)
		begin
			if (CLK = '1' and CLK'Event) then
				if RST = '1' then
					stCur <= stReceive;
				else
					stCur <= stNext;
				end if;
			end if;
		end process;
		

	process (stCur, rdaSig, dboutsig)
		begin
			case stCur is
				when stReceive =>
					rdSig <= '0';
					wrSig <= '0';

					if rdaSig = '1' then
						dbInSig <= dbOutSig;
						stNext <= stSend;
					else
						stNext <= stReceive;
					end if;			
				when stSend =>
					rdSig <= '1'; 
					wrSig <= '1';

					stNext <= stReceive;
			end case;
		end process;
end Behavioral;

