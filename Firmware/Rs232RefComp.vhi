
-- VHDL Instantiation Created from source file Rs232RefComp.vhd -- 12:42:24 07/28/2015
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT Rs232RefComp
	PORT(
		RXD : IN std_logic;
		CLK : IN std_logic;
		DBIN : IN std_logic_vector(7 downto 0);
		RD : IN std_logic;
		WR : IN std_logic;
		RST : IN std_logic;    
		RDA : INOUT std_logic;
		TBE : INOUT std_logic;      
		TXD : OUT std_logic;
		DBOUT : OUT std_logic_vector(7 downto 0);
		PE : OUT std_logic;
		FE : OUT std_logic;
		OE : OUT std_logic
		);
	END COMPONENT;

	Inst_Rs232RefComp: Rs232RefComp PORT MAP(
		TXD => ,
		RXD => ,
		CLK => ,
		DBIN => ,
		DBOUT => ,
		RDA => ,
		TBE => ,
		RD => ,
		WR => ,
		PE => ,
		FE => ,
		OE => ,
		RST => 
	);


