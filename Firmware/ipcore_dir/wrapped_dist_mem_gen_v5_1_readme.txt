The following files were generated for 'wrapped_dist_mem_gen_v5_1' in directory 
C:\Users\Patrizio\Desktop\VHDL_Projects\test_tensiometro_02_Luglio\ipcore_dir\

dist_mem_gen_ds322.pdf:
   Please see the core data sheet.

dist_mem_gen_readme.txt:
   Text file indicating the files generated and how they are used.

wrapped_dist_mem_gen_v5_1.asy:
   Graphical symbol information file. Used by the ISE tools and some
   third party tools to create a symbol representing the core.

wrapped_dist_mem_gen_v5_1.gise:
   ISE Project Navigator support file. This is a generated file and should
   not be edited directly.

wrapped_dist_mem_gen_v5_1.ngc:
   Binary Xilinx implementation netlist file containing the information
   required to implement the module in a Xilinx (R) FPGA.

wrapped_dist_mem_gen_v5_1.sym:
   Please see the core data sheet.

wrapped_dist_mem_gen_v5_1.v:
   Verilog wrapper file provided to support functional simulation.
   This file contains simulation model customization data that is
   passed to a parameterized simulation model for the core.

wrapped_dist_mem_gen_v5_1.veo:
   VEO template file containing code that can be used as a model for
   instantiating a CORE Generator module in a Verilog design.

wrapped_dist_mem_gen_v5_1.vhd:
   VHDL wrapper file provided to support functional simulation. This
   file contains simulation model customization data that is passed to
   a parameterized simulation model for the core.

wrapped_dist_mem_gen_v5_1.vho:
   VHO template file containing code that can be used as a model for
   instantiating a CORE Generator module in a VHDL design.

wrapped_dist_mem_gen_v5_1.xco:
   CORE Generator input file containing the parameters used to
   regenerate a core.

wrapped_dist_mem_gen_v5_1.xise:
   ISE Project Navigator support file. This is a generated file and should
   not be edited directly.

wrapped_dist_mem_gen_v5_1_readme.txt:
   Text file indicating the files generated and how they are used.

wrapped_dist_mem_gen_v5_1_xmdf.tcl:
   ISE Project Navigator interface file. ISE uses this file to determine
   how the files output by CORE Generator for the core can be integrated
   into your ISE project.

wrapped_dist_mem_gen_v5_1_flist.txt:
   Text file listing all of the output files produced when a customized
   core was generated in the CORE Generator.


Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

