----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:37:05 06/13/2014 
-- Design Name: 
-- Module Name:    Test_tensiometro - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Test_tensiometro is
    Port ( clk_50mhz : in std_logic;
	        input_wire : in std_logic;
	        input_HV : in std_logic;
			  g_1_out : out std_logic;
			  g_2_out : out std_logic;
			  gate_out1: out std_logic; 
			  gate_out2: out std_logic;
			  
			
			  bit_ch: out std_logic_vector(7 downto 0);
			  
			  bit_dac: out std_logic_vector(7 downto 0);			  
			
			  bit16: out std_logic;
			  
			  bitOVC: out std_logic;  -- Over Current Bit
				
			  wr_dac : out std_logic := '0';
			  
			  OVC : in std_logic;
			  
			  short_c1: in std_logic := '0';
			  short_c2: in std_logic := '0';
--			  short_c3: out std_logic := '0'; 
			  
----------------------------------------------------------seriale
           TXD : out std_logic := '1';
			  RXD : in std_logic := '1';
			  RST : in std_logic	:= '0'
			  );
end Test_tensiometro;

architecture Behavioral of Test_tensiometro is

   component clockMultiplier
	   port (
		    clk_in: in std_logic; --clock da 50MHz
			 input_hv: in std_logic;
			 reset : in std_logic;
                         gate_1 : out std_logic;
                         gate_2 : out std_logic
			 );
	 end component;		 

   component Counter
      port (
		    clock: in std_logic; --clock
			 input_wire: in std_logic;
			 G_1_IN: in std_logic;
			 G_2_IN: in std_logic;
			 reset: in std_logic; --reset
			 en: in std_logic := '1'; -- enable

			 outbit1 : out  STD_LOGIC_VECTOR (31 downto 0);
			 outbit2 : out  STD_LOGIC_VECTOR (31 downto 0);
			 valid1 : out std_logic;
			 valid2 : out std_logic
			 );
     end component;	
	  
	COMPONENT debouncer
	PORT(
		clk : IN std_logic;
		input : IN std_logic;
		busy : OUT std_logic;
		output : OUT std_logic
		);
	END COMPONENT;
	 
	 component CounterGate
		port ( 
		     clk_50m : in std_logic;
			  reset: in std_logic;
			  gate1 	: in std_logic;
			  gate2 : in std_logic;
			  count_gate1 : out std_logic_vector(31 downto 0);
			  count_gate2 : out std_logic_vector(31 downto 0);
			  valid_gate1 : out std_logic;
			  valid_gate2 : out std_logic
			  );
	end component;
	
	  
	  component RS232RefComp
     Port (  	
			TXD 	   : out	std_logic:= '1';
		 	RXD 	   : in	std_logic;					
  		 	CLK      : in	std_logic;							
			DBIN 	   : in	std_logic_vector (7 downto 0);
			DBOUT	   : out	std_logic_vector (7 downto 0);
			RDA	   : inout	std_logic;							
			TBE	   : inout	std_logic:= '1';				
			RD		   : in	std_logic;							
			WR		   : in	std_logic;							
			PE	    	: out	std_logic;							
			FE		   : out	std_logic;							
			OE	   	: out	std_logic;											
			RST	   : in	std_logic:= '0');				
     end component;	
	  
	  

	component wrapped_dist_mem_gen_v5_1
		port (
		a: in std_logic_vector(7 downto 0);
		d: in std_logic_vector(31 downto 0);
		dpra: in std_logic_vector(7 downto 0);
		clk: in std_logic;
		we: in std_logic;
		spo: out std_logic_vector(31 downto 0);
		dpo: out std_logic_vector(31 downto 0));
	end component;

		component Count_Gate_1
		port (
		a: in std_logic_vector(7 downto 0);
		d: in std_logic_vector(31 downto 0);
		dpra: in std_logic_vector(7 downto 0);
		clk: in std_logic;
		we: in std_logic;
		spo: out std_logic_vector(31 downto 0);
		dpo: out std_logic_vector(31 downto 0));
	end component;
	
	  	  
	  
type mainState is (stReceive,stSend);

signal dbInSig	:	std_logic_vector(7 downto 0);
signal dbOutSig :	std_logic_vector(7 downto 0);

signal rdaSig	:	std_logic;
signal tbeSig	:	std_logic;
signal rdSig	:	std_logic;
signal wrSig	:	std_logic;
signal peSig	:	std_logic;
signal feSig	:	std_logic;
signal oeSig	:	std_logic;

signal counter_clk: std_logic_vector(31 downto 0);
signal serial_clk : std_logic;
signal serial_clk_old : std_logic;

signal valid_gate1: std_logic;
signal valid_gate2: std_logic;
signal valid_count1: std_logic;
signal valid_count2: std_logic;
signal got_gate1: std_logic;
signal got_gate2: std_logic;
signal got_count1: std_logic;
signal got_count2: std_logic;

signal got_clear: std_logic;

signal count1_latch	:	std_logic_vector(31 downto 0);
signal count2_latch	:	std_logic_vector(31 downto 0);
signal gate1_latch	:	std_logic_vector(31 downto 0);
signal gate2_latch	:	std_logic_vector(31 downto 0);

	
signal stCur	:	mainState := stReceive;
signal stNext	:	mainState;	

signal sel1: std_logic := '1';
signal sel2: std_logic:= '0';
signal sel3: std_logic:= '0';
signal sel4: std_logic:= '0';  
	  	           			  	  
 --signal clock: std_logic;
signal clk1: std_logic;
 --signal tx_buf : std_logic_vector(7 downto 0);
signal g_1: std_logic;
signal g_2: std_logic;
signal wrt_1: std_logic := '0';
signal wrt_2: std_logic := '0';

signal cnt1: std_logic := '1';
signal cnt2: std_logic := '0';
signal cnt3: std_logic := '0';
signal cnt4: std_logic := '0';
signal flg: std_logic := '0';
signal flg2: std_logic := '0';
signal flg3: std_logic := '0';
signal flg4: std_logic := '0';

--signal cnt: integer range 0 to 5;
signal cnt: std_logic_vector(7 downto 0) := "00000000" ;
signal ContSig2	:	std_logic_vector(31 downto 0);  -- := "00000000000000000000000000000010";
signal ContSig1	:	std_logic_vector(31 downto 0);    --:= "00000000000000000000000001111010";

----------------------------Conteggio durata Gate--------------------------------

signal CountGate1	:	std_logic_vector(31 downto 0);
signal CountGate2	:	std_logic_vector(31 downto 0);

--signal shift_gate: std_logic_vector(7 downto 0); --shift gate

--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

signal data_in	:	std_logic_vector(31 downto 0);
signal mem_address	:	std_logic_vector(7 downto 0) := "00000000";
signal rd_mem_address : std_logic_vector(7 downto 0) := "00000000";
signal spo_out	:	std_logic_vector(31 downto 0);
signal dpo_out	:	std_logic_vector(31 downto 0);

signal mem_full	:	std_logic := '0';
signal rd_mem	:	std_logic := '0';
signal rd_mem_done	:	std_logic := '0';
signal strt_rd_mem	:	std_logic := '0';

signal data_wrote	:	std_logic := '0';
signal data1_wrote	:	std_logic := '0';
signal wrt_enable	:	std_logic := '0';
signal count_on	:	std_logic := '1';
signal wr_flg: std_logic := '0';

------%%%%%%%%%%%%%%%%%%%% Segnali memoria Gate %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
signal input_hv_debounced : std_logic;



signal gate_data_in	:	std_logic_vector(31 downto 0);
signal gate_spo_out	:	std_logic_vector(31 downto 0);
signal gate_dpo_out	:	std_logic_vector(31 downto 0);

signal reset_1 : std_logic := '0';
signal reset_cont : std_logic := '0';  -- per resettare il contatore
signal enable_cont : std_logic := '1'; -- per abilitare il contatore
signal reset_ser : std_logic := '0';  -- per reset seriale
signal readout_en : std_logic := '0';   -- per abilitare la stampa del conteggio su seriale
signal HV_ON : std_logic := '0';


begin

-- input debouncer to stabilised the input square wfm
	debouncer_inst: debouncer PORT MAP(
		clk => clk_50mhz,
		input => input_HV,
		busy => open,
		output => input_hv_debounced
	);
	
	gate_out1 <= input_wire;
	gate_out2 <= input_hv_debounced;
		
	clockMultiplier_inst: clockMultiplier
	port map (
		clk_in => clk_50mhz,
		reset => reset_cont,
		input_hv => input_hv_debounced,
		gate_1 => g_1,
		gate_2 => g_2		 
	);

-- inside we count tge input_wire... sensitive to wire oscillation
	Counter_inst: Counter
	port map (
		clock => clk_50mhz,
			input_wire => input_wire,
			G_1_IN => g_1,
			G_2_IN => g_2,
			reset => reset_cont,
			en => enable_cont,
			outbit1 => ContSig1,
			outbit2 => ContSig2,
			valid1 => valid_count1,
			valid2 => valid_count2
		);

-- inside we count the gate
	CounterGate_inst: CounterGate
	port map (
		clk_50m => clk_50mhz,
		reset => reset_cont,
		gate1 => g_1,
		gate2 => g_2,
		count_gate1 => CountGate1,
		count_gate2 => CountGate2,
		valid_gate1 => valid_gate1,
		valid_gate2 => valid_gate2
	);
			
-- generate flags for data readout: it is valid if received in the order:
-- gate1 then count1 then gate2 then count2			
	process (clk_50mhz)
	begin
		if(clk_50mhz'event and clk_50mhz = '1') then
			if (readout_en = '1' and got_clear='0') then
				if(valid_gate1 ='1') then
					got_gate1 <= '1';
				end if;
				if(valid_count1 ='1') then
					got_count1 <='1';
				end if;
				
				if(got_gate1 = '1' and valid_gate2 = '1') then
					got_gate2 <= '1';
				end if;
				
				if(got_count1 = '1' and valid_count2 = '1') then
					got_count2 <= '1';
				end if;
				
			else
				got_gate1 <= '0';
				got_gate2 <= '0';
				got_count1 <= '0';
				got_count2 <= '0';
			end if;
		end if;
	end process;			
		
-- serial port		 
	UART: RS232RefComp 
	port map (	
		TXD 	=> TXD,
		RXD 	=> RXD,
		CLK 	=> clk_50mhz,
		DBIN 	=> dbInSig,
		DBOUT	=> dbOutSig,
		RDA	=> rdaSig,
		TBE	=> tbeSig,	
		RD		=> rdSig,
		WR		=> wrSig,
		PE		=> peSig,
		FE		=> feSig,
		OE		=> oeSig,
		RST 	=> RST
	);
					
-- generater counter and serial clocks
	process (clk_50mhz)
	begin
		if(clk_50mhz'event and clk_50mhz = '1') then
			if(counter_clk(20)='1') then
				counter_clk <= X"00000001";
			else
				counter_clk <= counter_clk + 1;
			end if;
			
			serial_clk <= counter_clk(12);
			serial_clk_old <= serial_clk;
		end if;
	end process;

-- here the serial readout
	 process (clk_50mhz, reset_ser)
	  begin			
			if (clk_50mhz'event and clk_50mhz = '1') then
				if reset_ser = '1' then -- reset
					dbInSig <= (others => '0');
					cnt <= (others => '0');
					wrSig <= '0';
				else
					
						if wrSig = '1' then -- toggle uart write strobe to be 1 clk
							wrSig <= '0';
						end if;
							
						got_clear <='0';
						if (serial_clk='1' and serial_clk_old='0') then
							if (got_gate1= '1' and got_gate2 ='1' and got_count1 = '1' and got_count2='1')  then
								if wrSig = '0' then
									if cnt = 0 then
										-- latch
										count1_latch <= ContSig1;
										count2_latch <= ContSig2;
										gate1_latch <= CountGate1;
										gate2_latch <= CountGate2;
										dbInSig <= X"a5";
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 1 then
										dbInSig <= count1_latch(7 downto 0);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 2 then
										dbInSig <= count1_latch(15 downto 8);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 3 then
										dbInSig <= count1_latch(23 downto 16);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 4 then
										dbInSig <= count1_latch(31 downto 24);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 5 then
										dbInSig <= gate1_latch(7 downto 0);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 6 then
										dbInSig <= gate1_latch(15 downto 8);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 7 then
										dbInSig <= gate1_latch(23 downto 16);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 8 then
										dbInSig <= gate1_latch(31 downto 24);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 9 then
										dbInSig <= count2_latch(7 downto 0);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 10 then
										dbInSig <= count2_latch(15 downto 8);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 11 then
										dbInSig <= count2_latch(23 downto 16);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 12 then
										dbInSig <= count2_latch(31 downto 24);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 13 then
										dbInSig <= gate2_latch(7 downto 0);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 14 then
										dbInSig <= gate2_latch(15 downto 8);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 15 then
										dbInSig <= gate2_latch(23 downto 16);
										cnt <= cnt + 1;
										wrSig <= '1';
									elsif cnt = 16 then
										dbInSig <= gate2_latch(31 downto 24);
										cnt <= cnt + 1;
										wrSig <= '1';
									else
										wrSig <= '0';
										cnt <= (others => '0');
										got_clear <='1';
										
									end if;
								end if;
							--else
								-- mandare altra roba....
							end if;	-- strt_rd_mem
						end if;
					end if;

				end if;	-- reset
	end process;

-- data from serial interpreter
	process (clk_50mhz)     -------reset_ser, stamp_en
	variable count_reset : integer range 0 to 8 := 0;  ------------- to 8
	begin
		if clk_50mhz'event and clk_50mhz = '1' then
			if reset_ser = '1' then 
				if count_reset >= 5 then    -------- 5
					reset_ser <= '0';
					count_reset := 0;
				else 
					count_reset := count_reset + 1;
				end if;									
				
			else		
				rdSig <= '0';
					
				if rdaSig = '1' then
					if dbOutSig = "00101110" then   -- .
						reset_ser <= '1';
													
					elsif dbOutSig = "00111110" then  -- >
						readout_en <= '1';

					elsif dbOutSig = "00111011" then  --  ;
						readout_en <= '0';

							
 --         channel controls
							
					elsif dbOutSig = "00110000" then  -- 0						
						bit_ch <= "00000000"; 							 							
					elsif dbOutSig = "00110001" then  -- 1														
						bit_ch <= "00000001"; 																	
					elsif dbOutSig = "00110010" then  -- 2												
						bit_ch <= "00000010";							
					elsif dbOutSig = "00110011" then  -- 3												
						bit_ch <= "00000100";					
					elsif dbOutSig = "00110100" then  -- 4													
						bit_ch <= "00001000";						
					elsif dbOutSig = "00110101" then  -- 5												
						bit_ch <= "00010000";							
					elsif dbOutSig = "00110110" then  -- 6													
						bit_ch <= "00100000";							
					elsif dbOutSig = "00110111" then  -- 7												
						bit_ch <= "01000000";						
					elsif dbOutSig = "00111000" then  -- 8							
						bit_ch <= "10000000";							
					elsif dbOutSig = "00111001" then  -- 9														
						bit_ch <= "11111111";
							
--         DAC control

					elsif	dbOutSig = "01000001" then  --A																			
						bit_dac <= "00100000"; 							--32
					elsif dbOutSig = "01000010" then --B							
						bit_dac <= "01000000";                --64
					elsif dbOutSig = "01000011" then --C														
						bit_dac <= "01011111";						--95
					elsif dbOutSig = "01000100" then --D													
						bit_dac <= "01111111";						--127
					elsif dbOutSig = "01000101" then --E														
						bit_dac <= "10100000";						--160
					elsif dbOutSig = "01000110" then --F														
						bit_dac <= "10111111";						--191
					elsif dbOutSig = "01000111" then --G													
						bit_dac <= "11011111";						--223	
					elsif dbOutSig = "01001000" then --H														
						bit_dac <= "11111111";							--255	
							
					elsif dbOutSig = "00101000" then --(  DCDC ON
						bit16 <= '1';
						bitOVC <= '1';							
					elsif dbOutSig = "00101001" then --)  DCDC OFF
						bit16 <= '0';
						bitOVC <= '0';	
						
					end if;
					
					rdSig <= '1';
				end if;			
			end if;
		end if;
	end process;
----------------------------------------------------------------------

		--LED
		g_1_out <= g_1;
      g_2_out <= g_2;

		--old flags
		reset_cont <= '0';
		enable_cont <= '1';

		--always write DAC
		wr_dac <= '0';
end Behavioral;

