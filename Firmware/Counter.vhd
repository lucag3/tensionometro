----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:51:43 06/13/2014 
-- Design Name: 
-- Module Name:    Counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Counter is
    Port ( clock : in  STD_LOGIC;
	        input_wire: in std_logic;
	        G_1_IN: in std_logic;
			  G_2_IN: in std_logic;
           en : in  STD_LOGIC;
			  reset : in std_logic;
           outbit1 : out  STD_LOGIC_VECTOR (31 downto 0);
			  outbit2 : out  STD_LOGIC_VECTOR (31 downto 0);
			  valid1 : out std_logic;
			  valid2 : out std_logic
			  );
end Counter;

architecture Behavioral of Counter is

signal g1_old: std_logic;
signal g2_old: std_logic;

signal clr1: std_logic;
signal clr2: std_logic;

signal counter1: std_logic_vector(31 downto 0) := (others => '0');
signal write1: std_logic_vector(31 downto 0) := (others => '0');
signal write_valid1: std_logic;
signal counter2: std_logic_vector(31 downto 0) := (others => '0');
signal write2: std_logic_vector(31 downto 0) := (others => '0');
signal write_valid2: std_logic;


  function to_gray( Binary : std_ulogic_vector ) return std_ulogic_vector is
    alias b : std_ulogic_vector(Binary'length-1 downto 0) is Binary;
    variable result : std_ulogic_vector(b'range);
  begin
    result := b xor ('0' & b(b'high downto 1));

    return result;
  end function;

  function to_gray( Binary : std_logic_vector ) return std_logic_vector is
  begin
    return to_stdlogicvector(to_gray(to_stdulogicvector(Binary)));
  end function;

  function to_binary( Gray : std_ulogic_vector ) return std_ulogic_vector is
    alias g : std_ulogic_vector(Gray'length-1 downto 0) is Gray;
    variable result : std_ulogic_vector(g'range);
  begin
    result(result'high) := g(g'high);

    for i in g'high-1 downto 0 loop
      result(i) := g(i) xor result(i+1);
    end loop;

    return result;
  end function;

  function to_binary( Gray : std_logic_vector ) return std_logic_vector is
  begin
    return to_stdlogicvector(to_binary(to_stdulogicvector(Gray)));
  end function;

begin

gate_old: process(clock)
begin
	if (clock'event and clock = '1') then
		g1_old <= G_1_IN;
		g2_old <= G_2_IN;
		
		if(g1_old = '0' and G_1_IN = '1') then
			clr1 <= '1';
		else
			clr1 <= '0';
		end if;
		if(g2_old = '0' and G_2_IN = '1') then
			clr2 <= '1';
		else
			clr2 <= '0';
		end if;
	end if;
end process;

counter_gate1: process(input_wire, clr1)
begin
	if(clr1='1') then
		counter1 <=  (others => '0');
	elsif(input_wire'event and input_wire = '1') then
		if(G_1_IN='1') then
			counter1 <= counter1 + 1;
		end if;
	end if;
end process;

counter_gate2: process(input_wire, clr2)
begin
	if(clr2='1') then
		counter2 <=  (others => '0');
	elsif(input_wire'event and input_wire = '1') then
		if(G_2_IN='1') then
			counter2 <= counter2 + 1;
		end if;
	end if;
end process;

latch: process(clock)
begin
	if (clock'event and clock = '1') then
		if(g1_old = '1' and G_1_IN = '0') then
			write1 <= to_gray(counter1);
			write_valid1 <= '1';
		else
			write_valid1 <= '0';
		end if;
		
		if(g2_old = '1' and G_2_IN = '0') then
			write2 <= to_gray(counter2);
			write_valid2 <= '1';
		else
			write_valid2 <= '0';
		end if;
		
	end if;
end process;

outbit1 <= to_binary(write1);
outbit2 <= to_binary(write2);
valid1<=write_valid1;
valid2<=write_valid2;


end Behavioral;

