--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:55:15 06/13/2014
-- Design Name:   
-- Module Name:   C:/Users/Patrizio/Desktop/VHDL_Projects/Test_tensiometro/TestBench_Tensiometro.vhd
-- Project Name:  Test_tensiometro
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Test_tensiometro
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TestBench_Tensiometro IS
END TestBench_Tensiometro;
 
ARCHITECTURE behavior OF TestBench_Tensiometro IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Test_tensiometro
    PORT(
	      clk_50MHz : in std_logic;
	      input_wire : IN std_logic;
         input_HV : in std_logic;
			switch : in  STD_LOGIC_VECTOR (1 downto 0);
     --    Y : IN  bit;
  --       Z : out  STD_LOGIC_VECTOR (7 downto 0);
			gate_out1 : out std_logic;
		   gate_out2 : out std_logic;
			flag : out std_logic;
			sign_a: out std_logic;
	--		offs : out std_logic_vector( 31  downto 0);
			cont_1: out std_logic_vector(31  downto 0);
	--		cont_2: out std_logic_vector(31  downto 0);
	--		cont_3: out std_logic_vector(31  downto 0)
	      outbit1: out std_logic_vector(31  downto 0);
			outbit2: out std_logic_vector(31  downto 0);
			 clr_bf_1 : out std_logic;
			 clr_bf_2 : out std_logic
	--		 o_write1 : out  STD_LOGIC_VECTOR (31 downto 0);
	--		 o_write2 : out  STD_LOGIC_VECTOR (31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal input_HV_1 : std_logic;
	signal switch_1 : STD_LOGIC_VECTOR (1 downto 0);
  -- signal YY : bit := '0';
	signal input_wire_1 : std_logic;
	signal clk : std_logic;
	

 	--Outputs
 --  signal ZZ : STD_LOGIC_VECTOR (7 downto 0);
	signal gate_out1 : std_logic;
	signal gate_out2 : std_logic;
	signal flag : std_logic;
	signal sign_a : std_logic;
--	signal offs: std_logic_vector( 31  downto 0);
	signal cont_1: std_logic_vector(31  downto 0);
--	signal cont_2: std_logic_vector(31  downto 0);
--	signal cont_3: std_logic_vector(31  downto 0);
   signal outbit1: std_logic_vector(31  downto 0);
   signal outbit2: std_logic_vector(31  downto 0);
	signal clr_bf_1 :  std_logic;
	signal clr_bf_2 :  std_logic;
--	signal o_write1 :  STD_LOGIC_VECTOR (31 downto 0);
--	signal o_write2 :  STD_LOGIC_VECTOR (31 downto 0);
	
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
   constant clk_period : time := 20 ns;
   constant input_wire_period : time :=  1 us;
	constant input_HV_period : time := 20 ms;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Test_tensiometro PORT MAP (
	
	       input_wire => input_wire_1,
          input_HV => input_HV_1,       
			 switch => switch_1,
--			 Y => YY,
   --     Z => ZZ,
			 gate_out1 => gate_out1,
			 gate_out2 => gate_out2,
		--	 G_2 => G_2,
			 clk_50MHz => clk,
			 flag => flag,
			 sign_a => sign_a,
	--		 offs => offs,
			 cont_1 => cont_1,
	--		 cont_2 => cont_2,
	--		 cont_3 => cont_3
	       outbit1 => outbit1,
			 outbit2 => outbit2,
			 clr_bf_1 => clr_bf_1,       -----------------------
			 clr_bf_2 => clr_bf_2        -----------------------
		--	 o_write1 => o_write1,    -----------------------
		--	 o_write2 => o_write2
        );
		  
		  --clk definitions
		  clk_50MHz_process : process
		  begin
		   clk <= '0';
			wait for clk_period/2;
			clk <= '1';
			wait for clk_period/2;
			end process;
		    

   -- input_wire process definitions
	
   input_wire_process :process
   begin
		input_wire_1 <= '0';
		wait for input_wire_period/2;
		input_wire_1 <= '1';
		wait for input_wire_period/2;
   end process;
	
	 -- input_HV process definitions
	
	input_HV_process :process
   begin
		input_HV_1 <= '0';
		wait for input_HV_period/2;
		input_HV_1 <= '1';
		wait for input_HV_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
    --  wait for 10 us;	
		
		switch_1(1) <= '0';    -- enable contatore
		switch_1(0) <= '1';
		wait for 10 ms;
		
		switch_1(1) <= '1';
		switch_1(0) <= '0';     --reset contatore

  --    wait for clock_period*10;

      -- insert stimulus here 
    --   in_HV_1 <= '1'; YY <= '0'; wait for 100 us;
		-- in_HV_1 <= '1'; YY <= '1'; wait for 100 us;
   	-- in_HV_1 <= '1'; YY <= '0'; wait for 100 us;
      

      wait;
   end process;

END;
