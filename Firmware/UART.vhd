----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:18:23 06/19/2014 
-- Design Name: 
-- Module Name:    UART - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UART is
    Port ( TXD : out  STD_LOGIC;
           RXD : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           DBIN : in  STD_LOGIC;
           DBOUT : out  STD_LOGIC;
           RDA : in  STD_LOGIC;
           TBE : out  STD_LOGIC;
           RD : in  STD_LOGIC;
           WR : in  STD_LOGIC;
           PE : out  STD_LOGIC;
           FE : out  STD_LOGIC;
           OE : out  STD_LOGIC;
           RST : in  STD_LOGIC
			  );
end UART;

architecture Behavioral of UART is

begin


end Behavioral;

