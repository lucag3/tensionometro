----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:19:05 02/20/2020 
-- Design Name: 
-- Module Name:    debouncer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer is
    Port ( clk : in  STD_LOGIC;
           input : in  STD_LOGIC;
			  busy : out STD_LOGIC;
           output : out  STD_LOGIC);
end debouncer;

architecture Behavioral of debouncer is

signal counter : std_logic_vector (15 downto 0);
signal counting : std_logic;
signal input_old : std_logic;

signal reg0: std_logic;
signal reg1: std_logic;

begin

process (clk)
begin  
   if (clk'event and clk = '1') then
	reg0 <= input;
	reg1 <= reg0;
	input_old <= reg1;
      if (counting = '1') then
         counter <= counter + X"0001";
			if(counter = X"FFFF") then
				counting <= '0';
			end if;
      else
         output <= reg1;
			if (input_old /= reg1) then
				counting <= '1';
				counter <= X"0000";
			end if;
      end if;
   end if;
end process;

busy <= counting;
end Behavioral;

