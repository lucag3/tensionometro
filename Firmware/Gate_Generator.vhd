----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:43:02 06/16/2014 
-- Design Name: 
-- Module Name:    Gate_Generator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Gate_Generator is
    Port ( clk_50MHz : in std_logic;
	        input_HV : in  STD_LOGIC;
           Gate1 : out  STD_LOGIC;
           Gate2 : out  STD_LOGIC
			  );
end Gate_Generator;

architecture Behavioral of Gate_Generator is

signal G_1 : std_logic := '0';
signal G_2 : std_logic := '0';

begin
 
  clk1_proc: process
  
   variable count : integer :=1;
  
    begin
	 	
	 
	 if(input_HV'event and input_HV ='1') then
	 
	      count :=1;
			G_1 <='0';
			end if;
--		
	    if(clk_50MHz'event and clk_50MHz ='1') then		
		 
          count :=count+1;			 

          if(count = 600000) then
			--  if (input_HV = '1') then
							
		       G_1 <= '1';
		       G_2 <= '0';
			  end if;
		     if(count = 1800000) then
							
		       G_1 <= '0';
		       G_2 <= '1';
			  end if;		
			 end if;
	 
	 end process;
	 
	 Gate1 <= G_1;
	 Gate2 <= G_2;

end Behavioral;

