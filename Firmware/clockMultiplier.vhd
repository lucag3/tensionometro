----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:19:23 06/18/2014 
-- Design Name: 
-- Module Name:    clockMultiplier - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clockMultiplier is
    
    Port ( clk_in : in  STD_LOGIC;
	        input_hv : in std_logic;
			  reset : in std_logic;
--			  shift: in std_logic_vector(7 downto 0);
--           clk_1 : out  STD_LOGIC;
--           clk_2 : out  STD_LOGIC;
--           clk_3 : out  STD_LOGIC;
			  gate_1: out std_logic;
			  gate_2: out std_logic
--			  gate_out1: out std_logic;
--			  gate_out2: out std_logic;
--			  flag: out std_logic;
--			  sign_a: out std_logic;
		--	  offs: out std_logic_vector(31 downto 0);
--			  cont_1: out std_logic_vector(31 downto 0)
		--	  cont_2: out std_logic_vector(31 downto 0);
		--	  cont_3: out std_logic_vector(31 downto 0)
			  );
end clockMultiplier;

architecture Behavioral of clockMultiplier is

-- clock generation
signal count: std_logic_vector(31 downto 0);
signal s_clk_1: std_logic := '0';
signal s_clk_2: std_logic := '0';
signal s_clk_3: std_logic := '0';
signal s_clk_1_old: std_logic := '0';
signal s_clk_2_old: std_logic := '0';
signal s_clk_3_old: std_logic := '0';

signal input_hv_old: std_logic := '0';
signal count_high: std_logic_vector(31 downto 0) := (others => '0');
signal count_low: std_logic_vector(31 downto 0) := (others => '0');
signal gate: std_logic := '0';
	  
--	  signal s_gate_1: std_logic := '0';
--	  signal s_gate_2: std_logic := '0';
--	  signal sign_app: std_logic := '0';
--	  signal find_gate_strt: std_logic := '0';
--	  signal cont1: std_logic_vector(31 downto 0) := (others => '0');
--	  signal cont2: std_logic_vector(31 downto 0) := (others => '0');
--	  signal cont3: std_logic_vector(31 downto 0) := (others => '0');
--	  signal tot1: std_logic_vector(31 downto 0) := (others => '0');
--	  signal tot2: std_logic_vector(31 downto 0) := (others => '0');
--	  signal offset: std_logic_vector(31 downto 0) := (others => '0');
--	  signal cont_gate: std_logic_vector(7 downto 0) := (others => '0');
--	  signal flag_g: std_logic := '0';
--	  signal old_input_hv: std_logic := '0';
--	  signal flag_inv_g: std_logic := '0';
--	  signal flag_inv_g_out: std_logic := '0';
--	  signal due: std_logic_vector(31 downto 0) := "00000000000000000000000000000010";
--	  signal flg_res: std_logic := '0';
--	  signal cont_res: std_logic_vector(7 downto 0) := (others => '0');
--
--	  signal gate_1_on : std_logic := '0';
--	  signal gate_2_on : std_logic := '0';
--	  signal flag_cont1 : std_logic := '0';
--	  signal flag_cont2 : std_logic := '0';
	  
	  
     
begin

clk_proc: process(clk_in)
begin
	if(clk_in'event and clk_in = '1') then
		if(count(25)='1') then
			count <= X"00000001";
		else
			count <= count + X"00000001";
		end if;
		s_clk_1 <= count(8);
		s_clk_2 <= count(22);
		s_clk_3 <= count(25);
		s_clk_1_old <= s_clk_1;
		s_clk_2_old <= s_clk_2;
		s_clk_3_old <= s_clk_3;
	end if;
end process;

--     clk1_proc: process
--	  variable cont: integer := 1;
--	  begin 
--	    wait until clk_in'event and clk_in = '1';
----		 if (cont mod 2**15) = 0 then
--		 if (cont mod 2**8) = 0 then
--		   s_clk_1 <= not s_clk_1;
--		 end if;
--		 if (cont mod 2**22) = 0 then
--		   s_clk_2 <= not s_clk_2;
--	    end if;
--		 if (cont mod 2**25) = 0 then
--		   s_clk_3 <= not s_clk_3;
--		 end if;
--		 
--		 if cont = 2**25 then
--		    cont := 1;
--		 else
--          cont := cont + 1;
--       end if;
--		 
--    end process;

deay_input_hv: process(clk_in)
begin
	if(clk_in'event and clk_in = '1') then
		input_hv_old <= input_hv;
	end if;
end process;

time_high_proc: process(clk_in)
begin
	if(clk_in'event and clk_in = '1') then
		if(reset = '1') then
			count_high <= (others=>'0');
		else
			if(input_hv_old = '0' and input_hv='1') then
				-- on positive edge reset counter
				count_high <= (others=>'0');
			elsif (input_hv='1' and s_clk_1='1' and s_clk_1_old = '0') then
				count_high <= count_high + 1;
			end if;
		end if;
	end if;
end process;

time_low_proc: process(clk_in)
begin
	if(clk_in'event and clk_in = '1') then
		if(reset = '1') then
			count_low <= (others=>'0');
		else
			if(input_hv_old = '1' and input_hv='0') then
				-- on negative edge reset counter
				count_low <= (others=>'0');
			elsif (input_hv='0' and s_clk_1='1' and s_clk_1_old = '0') then
				count_low <= count_low + 1;
			end if;
		end if;
	end if;
end process;

combine_proc: process(clk_in)
begin
	if(clk_in'event and clk_in = '1') then
		if(reset = '1') then
			gate <= '0';
		else
			if(input_hv_old = '1') then
				-- high counter is counting
				if(count_high >= ('0' & count_low(31 downto 1))) then
					-- it reached half of the value of low counter
					gate <= '1';
				end if;
			else
				-- low counter is counting
				if(count_low >= ('0' & count_high(31 downto 1))) then
					-- it reached half of the value of high counter
					gate <= '0';
				end if;
			end if;
		end if;
	end if;
end process;


-- process (s_clk_1, reset, input_hv, cont1, cont2, cont3, flag_g, sign_app, gate_1_on, gate_2_on, tot1, tot2)
--
--	variable old_input_hv : std_logic := '0';
--
--   begin
--	  
--
--if (s_clk_1'event and s_clk_1 ='1') then 
--	if reset = '1' then
--	
--		cont1 <= (others=>'0');
--		cont2 <= (others=>'0');
--		cont3 <= (others=>'0');
--				
--		tot1 <= (others=>'0');
--		tot2 <= (others=>'0');
--		
--		flag_g <= '0';
--		sign_app <= '0';
--
--		s_gate_1 <= '0';
--		s_gate_2 <= '0';
--		gate_2_on <= '0';
--		gate_1_on <= '0';
--
--
--	
--	else
--     
--			if old_input_hv = '0' and input_hv = '1' then
--
--					flag_g <= '0';
--					cont1 <= (others => '0');
--
--					old_input_hv := '1';
----	
--			elsif old_input_hv = '1' and input_hv = '0' then
--					cont2 <= (others => '0');
--
--					flag_g <= '1';
--				   old_input_hv := '0';
--										
--			elsif input_hv = '1' then
--					cont1 <= cont1 + 1;	
--					old_input_hv := '1';    -- qui cont2 a zero
--			
--					
--			elsif input_hv = '0' then
--					cont2 <= cont2 + 1;		
--					old_input_hv := '0';	
--				
--			end if;
--			
--			
--			
--	      if flag_g = '1' then
--				if  cont2 >= (('0' & cont1(31 downto 1))  )  then   --00000000000000000001011111101111
----				if  cont2 >= 00000000000000000000000000000000  then
--					s_gate_1 <= '1';
--					s_gate_2 <= '0';
--					
----				if  cont2 >= (('0' & cont1(31 downto 1)) - 65 ) and cont2 < (('0' & cont1(31 downto 1)) + 65 )  then 
----				    s_gate_2 <= '1';
----					 s_gate_1 <= '0';
----					else
----					 s_gate_2 <= '0';
----					 s_gate_1 <= '0';
--				end if;
--
--			end if;
--			if flag_g = '0' then
--				if  cont1 >= (('0' & cont2(31 downto 1))  ) then
----				if  cont1 >= 00000000000000000000000000000000 then 
--					 s_gate_1 <= '0'; --'1';
--					 s_gate_2 <= '1';
--					 
----				if  cont1 >= (('0' & cont2(31 downto 1)) - 65 ) and cont1 < (('0' & cont2(31 downto 1)) + 65 ) then
----					 s_gate_1 <= '1';
----					 s_gate_2 <= '0';
----					else
----					 s_gate_1 <= '0';
----					 s_gate_2 <= '0';
--				end if;
--
--			end if;
--				
--	end if;
--end if;		
--	
--   end process;



	
--	 clk_1 <= s_clk_1;
--    clk_2 <= s_clk_2;
--    clk_3 <= s_clk_3;	
--    gate_1 <= s_gate_1;	 
--	 gate_2 <= s_gate_2;	 
--	 gate_out1 <= s_gate_1;	 
--	 gate_out2 <= s_gate_2;

--	 flag <= flag_g;

--	 sign_a <= sign_app;

--	 cont_1 <= cont1;

gate_1 <= gate;
gate_2 <= not gate;
--gate_2 <= gate;
		 
end Behavioral;		  



