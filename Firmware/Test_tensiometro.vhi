
-- VHDL Instantiation Created from source file Test_tensiometro.vhd -- 11:35:54 07/28/2015
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT Test_tensiometro
	PORT(
		clk_50mhz : IN std_logic;
		input_wire : IN std_logic;
		input_HV : IN std_logic;
		short_c1 : IN std_logic;
		short_c2 : IN std_logic;
		RXD : IN std_logic;
		RST : IN std_logic;          
		g_1_out : OUT std_logic;
		g_2_out : OUT std_logic;
		gate_out1 : OUT std_logic;
		gate_out2 : OUT std_logic;
		bit0 : OUT std_logic;
		bit1 : OUT std_logic;
		bit2 : OUT std_logic;
		bit3 : OUT std_logic;
		bit4 : OUT std_logic;
		bit5 : OUT std_logic;
		bit6 : OUT std_logic;
		bit7 : OUT std_logic;
		TXD : OUT std_logic
		);
	END COMPONENT;

	Inst_Test_tensiometro: Test_tensiometro PORT MAP(
		clk_50mhz => ,
		input_wire => ,
		input_HV => ,
		g_1_out => ,
		g_2_out => ,
		gate_out1 => ,
		gate_out2 => ,
		bit0 => ,
		bit1 => ,
		bit2 => ,
		bit3 => ,
		bit4 => ,
		bit5 => ,
		bit6 => ,
		bit7 => ,
		short_c1 => ,
		short_c2 => ,
		TXD => ,
		RXD => ,
		RST => 
	);


