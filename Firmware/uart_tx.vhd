----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:11:13 06/19/2014 
-- Design Name: 
-- Module Name:    uart_tx - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity uart_tx is

    generic (
	           fullbit : integer := 434
				  );
		       
    Port ( clk : in  STD_LOGIC;
           res_tx : in  STD_LOGIC;
           din : in  STD_LOGIC_VECTOR (7 downto 0);
           wr : in  STD_LOGIC;
           busy : out  STD_LOGIC;
           txd : out  STD_LOGIC);
end uart_tx;

architecture Behavioral of uart_tx is

constant halfbit : integer := fullbit/2;
signal bitcount : integer range 0 to 7;
signal count : integer range 0 to fullbit;
signal shiftreg : std_logic_vector(7 downto 0);

begin
  
   proc: process(clk, res_tx)
	  begin
	       if res_tx = '1' then
			   
				  count <= 0;
				  bitcount <= 0;
				  busy <= '0';
				  txd <= '1';
			elsif clk'event and clk = '1' then
			    if count/= 0 then
				    count <= count -1;
				 else  
                  if bitcount = 0 then
                        busy <= '0';
                        if wr = '1' then
                            shiftreg <= din;
                            busy <= '1';
                            txd <= '0';
                            bitcount <= bitcount +1;
                            count <= fullbit;
                        end if;
                  elsif bitcount = 9 then
						        txd <= '1';
								  bitcount <= 0;
								  count <= fullbit;
						else
                      
                          shiftreg(6 downto 0) <= shiftreg(7 downto 1);
                          txd <= shiftreg(0);
                          bitcount <= bitcount +1;
                          count <= fullbit;
                  end if;
				end if;
         end if;
end process;
end Behavioral;

