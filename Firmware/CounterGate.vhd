----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:26:17 07/22/2014 
-- Design Name: 
-- Module Name:    CounterGate - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CounterGate is
    Port ( clk_50m : in  STD_LOGIC;
           gate1 : in  STD_LOGIC;
			  reset : in std_logic;			
           gate2 : in  STD_LOGIC;
           count_gate1 : out  STD_LOGIC_VECTOR (31 downto 0);
           count_gate2 : out  STD_LOGIC_VECTOR (31 downto 0);
			  valid_gate1 : out std_logic;
			  valid_gate2 : out std_logic
	);
end CounterGate;

architecture Behavioral of CounterGate is

-- clock generation
signal count: std_logic_vector(31 downto 0);
signal s_clk_1: std_logic := '0';
signal s_clk_2: std_logic := '0';
signal s_clk_3: std_logic := '0';
signal s_clk_1_old: std_logic := '0';
signal s_clk_2_old: std_logic := '0';
signal s_clk_3_old: std_logic := '0';

signal o_buffer1: std_logic_vector(31 downto 0) := (others => '0');
signal o_write1: std_logic_vector(31 downto 0) := (others => '0');
signal o_write_valid1: std_logic;
signal o_buffer2: std_logic_vector(31 downto 0) := (others => '0');
signal o_write2: std_logic_vector(31 downto 0) := (others => '0');
signal o_write_valid2: std_logic;

signal gate1_old: std_logic:='0';
signal gate2_old: std_logic:='0';
begin

clk_proc: process(clk_50m)
begin
	if(clk_50m'event and clk_50m = '1') then
		if(count(25)='1') then
			count <= X"00000001";
		else
			count <= count + X"00000001";
		end if;
		s_clk_1 <= count(3);
		s_clk_2 <= count(22);
		s_clk_3 <= count(25);
		s_clk_1_old <= s_clk_1;
		s_clk_2_old <= s_clk_2;
		s_clk_3_old <= s_clk_3;
	end if;
end process;


-- gate 1 lenght counter
sync1: process(clk_50m)
begin
	if(clk_50m'event and clk_50m = '1') then
		gate1_old <= gate1;
		if(reset = '1') then
			o_buffer1 <= (others => '0');
			o_write_valid1 <= '0';
		else
			if(gate1_old='0' and gate1='0') then
				--reset buffer
				o_buffer1 <= (others => '0');
				o_write_valid1 <= '0';
			end if;
			
			if(gate1_old='1' and gate1='0') then
				--latch buffer
				o_write1 <= o_buffer1;
				o_write_valid1 <= '1';
			end if;
			
			if (gate1='1' and s_clk_1='1' and s_clk_1_old='0') then
				--if gate active and posedge slow clock
					o_buffer1 <= o_buffer1 + 1;
					o_write_valid1 <= '0';
			end if;
		end if;
	end if;
end process;

-- gate 2 lenght counter
sync2: process(clk_50m)
begin
	if(clk_50m'event and clk_50m = '1') then
		gate2_old <= gate2;
		if(reset = '1') then
			o_buffer2 <= (others => '0');
			o_write_valid2 <= '0';
		else
			if(gate2_old='0' and gate2='0') then
				--reset buffer
				o_buffer2 <= (others => '0');
				o_write_valid2 <= '0';
			elsif(gate2_old='1' and gate2='0') then
				--latch buffer
				o_write2 <= o_buffer2;
				o_write_valid2 <= '1';
			elsif (gate2='1' and s_clk_1='1' and s_clk_1_old='0') then
				o_buffer2 <= o_buffer2 + 1;
				o_write_valid2 <= '0';
			else
				o_write_valid2 <= '0';
			end if;
		end if;
	end if;
end process;


 count_gate1 <= o_write1; -- assegna lo stato all'uscita
 count_gate2 <= o_write2;

 valid_gate1 <= o_write_valid1;
 valid_gate2 <= o_write_valid2;

end Behavioral;

