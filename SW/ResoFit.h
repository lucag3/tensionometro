// Variables to be read and calculated
Double_t freq, c1, c2, g1, g2;
Double_t oldfreq = -1000.;
Double_t delta, devstd;

// Maximum number of files
static const Int_t nmaxfiles = 10;

// Maximum number of frequencies
static const Int_t nfreqmax = 1000;

// Vectors for graphs and histos
Double_t vnmeas[nmaxfiles][nfreqmax];
Double_t dvnmeas[nmaxfiles][nfreqmax] = {1.};
Double_t svnmeas[nfreqmax], sdvnmeas[nfreqmax];
Double_t vfreq[nmaxfiles][nfreqmax], diff[nmaxfiles][nfreqmax],
  ddiff[nmaxfiles][nfreqmax];
Double_t dvfreq[nmaxfiles][nfreqmax] = {0.001};
Double_t svfreq[nfreqmax], sdiff[nfreqmax], sddiff[nfreqmax];
Double_t sdvfreq[nfreqmax] = {0.001};

// Same vectors for graphs and histos for bkg
Double_t vnmeasbkg[nmaxfiles][nfreqmax];
Double_t dvnmeasbkg[nmaxfiles][nfreqmax] = {1.};
Double_t svnmeasbkg[nfreqmax], sdvnmeasbkg[nfreqmax];
Double_t vfreqbkg[nmaxfiles][nfreqmax], diffbkg[nmaxfiles][nfreqmax],
  ddiffbkg[nmaxfiles][nfreqmax];
Double_t dvfreqbkg[nmaxfiles][nfreqmax] = {0.001};
Double_t svfreqbkg[nfreqmax], sdiffbkg[nfreqmax], sddiffbkg[nfreqmax];
Double_t sdvfreqbkg[nfreqmax] = {0.001};

// Boolean variables to identify outliers
Bool_t c1bad = kFALSE;
Bool_t c2bad = kFALSE;
Bool_t g1bad = kFALSE;
Bool_t g2bad = kFALSE;

// Counter of read frequencies
Int_t ifreq = 0;

// Number of files
Int_t nfiles = 0;

// Counter of measurements
Int_t nmeasure = 0;

// Extreme values of read frequencies
Double_t minfreq = -1000., maxfreq = -1000.;

// Frequency range
Double_t fRange = -1000.;

// Running average to remove outliers
Double_t runavec1 = 0., runavec2 = 0.;
Double_t runaveg1 = 0., runaveg2 = 0.;

// Number of removed outliers
Int_t nremoutliers = 0;

// Counter of read lines
Int_t iread = 0; 

// Fit parameters
Double_t fitrespar[3][nmaxfiles], fitreserr[3][nmaxfiles], fitbkgpar[6][nmaxfiles];
Double_t tCnst, tErCnst, tMu, tErMu, tSgm, tErSgm; 

// Quality check histos
TH1F *hc1[nmaxfiles];
TH1F *hc2[nmaxfiles];
TH1F *ga1[nmaxfiles];
TH1F *ga2[nmaxfiles];
