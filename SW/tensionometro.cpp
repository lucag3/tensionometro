#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <stdio.h>    // Standard input/output definitions 
#include <unistd.h>   // UNIX standard function definitions 
#include <fcntl.h>    // File control definitions 
#include <errno.h>    // Error number definitions 
#include <termios.h>  // POSIX terminal control definitions 
#include <string.h>   // String function definitions 
#include <sys/ioctl.h>
#include "sdg2000X.h"
#include "serialdriver.h"
#include <time.h>

/*#include "TH1.h"
#include "TCanvas.h"*/

#pragma pack(push)
#pragma pack(1)
typedef struct{
   unsigned char dummy;
   unsigned int counts1;
   unsigned int gate1;
   unsigned int counts2;
   unsigned int gate2;
} datatype;
#pragma pack(pop)


int main(int argc, char ** argv) {

  if(argc!=2 && argc!=3) {
    printf(" Please indicate a wire number!\n");
    return -1;
  }

   //save the input parameters:
   // first parameter is the first wire under test	
   // second is (if present) the last wire, if absent only first wire is used, if less than first returns with error
   int firstwire = atoi(argv[1]);
   int secondwire;
   if(argv[2]!=NULL)
     secondwire = atoi(argv[2]);
   else 
     secondwire = firstwire;
   if(secondwire<firstwire) {
     printf(" ERROR: second parameter must be larger than first!!!!\n");
     return -1;
   }
   if(firstwire<1 || firstwire>8) {
     printf(" ERROR: first parameter minimum value is 1 and maximum is 8!!!!\n");
     return -1;
   }
   if(secondwire>8) {
     printf(" ERROR: second parament maximum value is 8!!!!\n");
     return -1;
   }

   // save the date!
   time_t date = time(NULL);
   struct tm tm = *localtime(&date);
   
   // connect to pulse generator 
   sdg2000X sdg("192.168.2.53");
   printf("Connected to pulser.\n");
   // set output voltage
   sdg.setAmplitude(1, 2.8);
   // set Sine wave
   sdg.setType(1, cSine);
   // set output load to 50 ohm
   sdg.chnLoad(1, c50Ohm);
   // enable the output if OFF
   if(sdg.chCheckOn(1) == false) {
     sdg.chnOn(1);
   }
   
   //connect to Arduino
   int arduino = serialport_init("/dev/tty.usbmodem14101",9600);	
   if(arduino < 0) {
       printf("error\n");
       return -1;
   }
   printf("Connected to Arduino.\n");

   //toggle DTR to reset Arduino
   int DTR_flag;
   DTR_flag = TIOCM_DTR;
   ioctl(arduino,TIOCMBIS,&DTR_flag);//Set DTR pin
   sleep(1);
   ioctl(arduino,TIOCMBIC,&DTR_flag);//Clear DTR pin
   sleep(1);

   char arduino_buf[128];

   //connect to USB
   int serial = serialport_init("/dev/tty.SLAB_USBtoUART",115200);	
   printf("Connected to serial.\n");
   int ndata;
   // timeout in ms
   int timeout = 2000;
   // number of events to read (50)
   const int datatoread = 17*50;
   char data[datatoread];
   // loop on wires
   for(int iwire = firstwire; iwire<secondwire+1; iwire++) {
   // open outout file
   char filename[100];
   sprintf(filename,"data/out%d-%d-%02d-%02d-%02d.dat", iwire,tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour);
   FILE * fout = fopen(filename, "w");
   printf("-------------------\n");
   printf("Wire %d under test.\n",iwire);
   printf("-------------------\n");

   //request an Arduino measurement
   int ret_arduino = serialport_writebyte(arduino,'0');

   //temperature
   ret_arduino = serialport_readline(arduino,arduino_buf, 128, 2000);
   for(int i=0; i<strlen(arduino_buf); i++) if(arduino_buf[i]== '\n' || arduino_buf[i]== '\r') arduino_buf[i] = '\0';
   printf("Temp: %s ", arduino_buf);
   fprintf(fout, "# Temp: %s\n", arduino_buf);

   //pressure
   ret_arduino = serialport_readline(arduino,arduino_buf, 128, 2000);
   for(int i=0; i<strlen(arduino_buf); i++) if(arduino_buf[i]== '\n' || arduino_buf[i]== '\r') arduino_buf[i] = '\0';
   printf("Press: %s ", arduino_buf);
   fprintf(fout, "# Press: %s\n", arduino_buf);

   //humidity
   ret_arduino = serialport_readline(arduino,arduino_buf, 128, 2000);
   for(int i=0; i<strlen(arduino_buf); i++) if(arduino_buf[i]== '\n' || arduino_buf[i]== '\r') arduino_buf[i] = '\0';
   printf("Humidity: %s\n", arduino_buf);
   fprintf(fout, "# Humidity: %s\n", arduino_buf);

   //additional new line
   ret_arduino = serialport_readline(arduino,arduino_buf, 128, 2000);

   // enable chosen wire
   serialport_writebyte(serial,'0'+iwire);
   // choose HV level on the PCB
   serialport_writebyte(serial,'G');
   // swith ON HV
   serialport_writebyte(serial,'(');
   //serialport_writebyte(serial,')');
   // stop data trasmission from PCB fro safety
   serialport_writebyte(serial,';');
   // flush the buffer to removed unwanted data 
   do{
     ndata = serialport_read_until(serial,data,datatoread,1000);
   } while (ndata>0);
   
   // set first frequency to 50
   float freq=40;
   sdg.setFreq(1, freq);
   usleep(1000000);
   
   // loop on measurements
   for(int iloop=0; iloop <80; iloop++){
     printf("\n Current frequency %f\n", freq);
     int meas=0;
     double sum=0;
     double sum2=0;
     // set data to 0
     memset(data, 0, sizeof(data));
     // enable data transmission
     serialport_writebyte(serial,'>');
     // read data
     ndata = serialport_read_until(serial,data,datatoread,timeout);
     // message in case of timeout during data readout
     if(ndata == -2)
       printf("troubles in reading data! Got timeout from serial port!\n");
     // stop data transmission
     serialport_writebyte(serial,';');
     
     // data readout and save to file
     if(ndata==datatoread){ 
       for(int i=0; i<ndata; i += 17){
	 datatype *ptr = (datatype*) (data + i);
	 double ratio1 = ptr->counts1*1./ptr->gate1;
	 double ratio2 = ptr->counts2*1./ptr->gate2;
	 
	 fprintf(fout, "%lf %u %u %u %u\n", freq, ptr->counts1, ptr->counts2, ptr->gate1, ptr->gate2);
	 
	 sum += ratio2-ratio1;
	 sum2 += (ratio2-ratio1)*(ratio2-ratio1);
	 //check that the actual frequency is compatible with pulser's
	 double freq_ro =  1./(20e-9*16*(ptr->gate1+ptr->gate2)) ;
	 double diff_freq = freq - freq_ro;
	 if(meas==0 && abs(diff_freq)>1.){
	   printf("troubles with pulser and data synchronisation:\n readout frequecy = %lf\n",freq_ro);
	   return -1;
	 } 
	 meas++;
       }
     }
     freq += 20./80;
     // setup of next frequency plus detay
     sdg.setFreq(1, freq);
     usleep(300000);
     serialport_read_until(serial,data,datatoread,100);
     printf("measurements: %d, mean: %.10lf, rms: %le\n", meas, sum/meas, sqrt((sum2/meas)-(sum*sum/(meas*meas))));
   }// end for iloop
   // close current file
   fclose(fout);
   }// end for iwire
   // close connection to serial 
   // swith OFF HV
   serialport_writebyte(serial,')');
   serialport_close(serial);
   sdg.chnOff(1);
   return 0;  
}
