#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <arpa/inet.h>
#define PORT 5024

enum eWfmType {
   cSine,
   cSquare
};

enum eOutLoad {
   cHiZ,
   c50Ohm
};

class sdg2000X {
 public:
  sdg2000X(char* ip);
  void sendcmd(char* cmd);
  void receivedata(char* buf, int size);

  void setAmplitude(int ch, float val);
  void setFreq(int ch, float val);
  void setType(int ch, eWfmType val);

  void chnOn(int ch);
  void chnOff(int ch);
  bool chCheckOn(int ch);

  void chnLoad(int ch, eOutLoad val);

 private:
  char fIp[32];
  int fSocket;
};


