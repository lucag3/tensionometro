#include "sdg2000X.h"

sdg2000X::sdg2000X(char* ip){
   strcpy(fIp, ip);

   if ((fSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
   {
        printf("\n Socket creation error \n");
        exit(1);
   }

   struct sockaddr_in serv_addr;
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_port = htons(PORT);

   // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, fIp, &serv_addr.sin_addr)<=0)
    {
        printf("\nInvalid address/ Address not supported \n");
        exit(1);
    }

    if (connect(fSocket, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        exit(1);
    }

    char buffer[1024];
    receivedata(buffer, sizeof(buffer));

    char idn[] = "*IDN?";
    sendcmd(idn);

    //receivedata(buffer, sizeof(buffer));

    printf("connected to %s at %s\n", buffer, fIp);
}

void sdg2000X::sendcmd(char* cmd){
   send(fSocket, cmd, strlen(cmd), 0 );
   char buf[100];
   receivedata(buf, sizeof(buf));
}

void sdg2000X::receivedata(char* buf, int size){
   int nread;
   nread = read(fSocket, buf, size);
   buf[nread-1] = 0;
}

void sdg2000X::setAmplitude(int ch, float val){
   char buf[1024];
   sprintf(buf, "C%1d:BSWV AMP,%f\n", ch, val);
   sendcmd(buf);
}

void sdg2000X::setFreq(int ch, float val){
   char buf[1024];
   sprintf(buf, "C%1d:BSWV FRQ,%f\n", ch, val);
   sendcmd(buf);
}

void sdg2000X::setType(int ch, eWfmType val){
   char type[10];
   if(val==cSine){
      strcpy(type, "SINE");
   } else if(val==cSquare) {
      strcpy(type, "SQUARE");
   } else {
      printf("invalid waveform type\n");
      return;
   }

   char buf[1024];
   sprintf(buf, "C%1d:BSWV WVTP,%s\n", ch, type);
   sendcmd(buf);
}

void sdg2000X::chnOn(int ch){
   char buf[1024];
   sprintf(buf, "C%1d:OUTP ON\n", ch);
   sendcmd(buf);
}

void sdg2000X::chnOff(int ch){
   char buf[1024];
   sprintf(buf, "C%1d:OUTP OFF\n", ch);
   sendcmd(buf);
}

void sdg2000X::chnLoad(int ch, eOutLoad val){
   char type[10];
   if(val==cHiZ){
      strcpy(type, "HZ");
   } else if(val==c50Ohm) {
      strcpy(type, "50");
   } else{
      printf("invalid output load\n");
      return;
   }
   char buf[1024];
   sprintf(buf, "C%1d:OUTP LOAD,%s\n", ch, type);
   sendcmd(buf);
}

bool sdg2000X::chCheckOn(int ch){
  char cmd[100];
  sprintf(cmd,"C%1d:OUTP?\n",ch);
  send(fSocket, cmd, strlen(cmd), 0 );
  char buf[100];
  usleep(100);
  receivedata(buf, sizeof(buf));
  receivedata(buf, sizeof(buf));
  char on[20] = "ON";
  // check the string searching for ON
  if(strncmp(buf+8,on,2) == 0)
    return true;
  else
    return false;
}
