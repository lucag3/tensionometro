#include "sdg2000X.h"

int main(){
   sdg2000X sdg("192.168.2.53");
   //sdg2000X sdg("192.168.2.1");
   
   sdg.setAmplitude(1, 3);
   sdg.setFreq(1, 41.2);
   sdg.setType(1, cSine);
   sdg.chnLoad(1, c50Ohm);
   sdg.chnOn(1);
   getchar();
   sdg.chnOff(1);
}
